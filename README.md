# lopi

LOpi stands for LocalOscillator driven by a RaspberryPi computer. 

It is meant to be a generic architecture for a digitally controlled LO,
taking advantage of good analog components & relaxed digital constraints.

Current design works with:
* PID loop run in Python3 on a RaspberryPi 3B+,
* Actuator is a DAC, 16 bit AD5541A, SPI,
* Feedback is LAN-based frequency data, from FXE by K+K,


